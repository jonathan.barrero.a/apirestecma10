import express, { json } from 'express'


const app = express()

//routes
import IndexRoutes from './routes/index.routes'
import TaskRoutes from './routes/task.route' 
//settings

app.set('port', process.env.PORT || 3000)


//middleware
app.use(json())

app.use(IndexRoutes)
app.use('/tasks', TaskRoutes)

export default app