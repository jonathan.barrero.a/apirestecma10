import { Router } from 'express'

const router = Router()

//Database conection
import { connect } from '../database'



router.get('/', async (req,res) => {
    const db = await connect()
    const result = await db.collection('task').find({}).toArray()
    console.log(result)
    res.json(result)
})

router.post('/', async (res,res) => {
    const db = await connect()
    const task = {
        tittle : req.body.tittle,
        description: req.body.description
    } 
    res.send('Task created')
})

export default router